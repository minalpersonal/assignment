package com.poker.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.poker.game.Card;

public class ValidationUtil {
	

	static final List<String> VALID_CARD_VALUES = new ArrayList<String>(
			Arrays.asList("A", "2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K"));
	static final List<String> VALID_SUIT_VALUES = new ArrayList<String>(Arrays.asList("D", "H", "S", "C"));
	
	public static boolean validateHands(List<String> cards, List<Card> cardsDealt) {
		
		if(cards.size()!=10) {
			return false;
		}
		
		return validateCards(cards, cardsDealt);
	
	}

	private static boolean validateCards(List<String> cards, List<Card> cardsDealt) {
		boolean valid = false;
		for (String card : cards) {
			Card fetchedCard = validateAndGetCard(card);
			if (fetchedCard!=null) {
				cardsDealt.add(fetchedCard);
				valid = true;
				continue;
			} else {
				cardsDealt = null;
				valid =false;
				break;
			}
		}
		return valid;
	}
	
	public static Card validateAndGetCard(String card) {
		if (card.length() < 2) {
			return null;
		}

		String value = card.substring(0, 1);
		String suit = card.substring(1, card.length());
		if (VALID_CARD_VALUES.contains(value) && VALID_SUIT_VALUES.contains(suit)) {
				return new Card(Card.getCardIntValue(value), suit);
		}else {
			return null;
		}
		
	}

}
