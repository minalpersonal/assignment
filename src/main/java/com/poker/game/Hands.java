package com.poker.game;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Checks the hands 
 * @author 
 *
 */
public class Hands {

	private static final Logger LOGGER = LoggerFactory.getLogger(Hands.class);
	
	public static int checkHand(List<Card> hand){
      
		Collections.sort(hand);
        
        if (royalFlush(hand)==1){
           LOGGER.debug("  :Royal Flush");
            return 10;
        }
        else if (straightFlush(hand)==1){
        	LOGGER.debug("  :Straight flush");
        	return 9;
        }
        else if (four(hand)==1){
        	LOGGER.debug("  :Four of a kind");
        	return 8;
        }
        else if(fullHouse(hand)==1){
        	LOGGER.debug("  :Full House");
        	return 7;
        }
        else if (flush(hand)==1){
        	LOGGER.debug("  :Flush");
        	return 6;
        }
        else if (straight(hand)==1){
        	LOGGER.debug("  :Straight");  
        	return 5;
        }
        else if (three(hand)==1){
        	LOGGER.debug("  :Three of a kind");
        	return 4;
        }
        else if (twoPairs(hand)==1){
        	LOGGER.debug("  :2 pairs");
        	return 3;
        }
        else if (pair(hand)==1){
        	LOGGER.debug("  :1 pair");
        	return 2;
        }
        else{
        	LOGGER.debug("  :0 Pairs");
        	return 1;
        }
    }
	
	/**
	 * Count number of pairs
	 * @param hand
	 * @return
	 */
	public static int pair(List<Card> hand){
   
		Set<Integer> common = new HashSet<Integer>();
		int numberPairs=0; 
		int firstCommon =0;
		for(Card card: hand) {
			
			if(!common.add(card.getValue())) {
				if(numberPairs==0) {
				firstCommon = card.getValue();
				numberPairs++;
				}else if(firstCommon!=card.getValue()){
					numberPairs++;
				}
				
			}
			common.add(card.getValue());
		}
        return numberPairs;
    }
    
    public static int twoPairs(List<Card> hand){
        if(pair(hand)==2){
            return 1;   
        }
        else{
            return 0;
        }
    }
    
    public static int three(List<Card> hand){
        if (pair(hand)==0){
            return 0;    
        }
        //Either of these pairs have to be similar in 5 card hand
        else if( hand.get(0).isSameValue(hand.get(2)) || hand.get(1).isSameValue(hand.get(3)) || hand.get(2).isSameValue(hand.get(4))){
                return 1;
            }
        else {
            return 0;
        }
    }
    
    public static int straight(List<Card> hand){

    	//check  for base condition of no pair and difference
        if (pair(hand) > 0 || (hand.get(4).getValue()-hand.get(0).getValue() != 4)){
            // the hand can't have a pair
            return 0;
        }
        
        int i=0;
        while(i<=hand.size()-2) {
        	if(Card.areConsecutive(hand.get(i), hand.get(i+1))) {
        		i++;
        		continue;
        		
        	}else {
        		return 0;
        	}
        }
        return 1;  
    }
    
    public static int flush(List<Card> hand){
        //check all card's suits
            int i=1;
            while (i<=hand.size()-2) {
            	if(hand.get(i).getSuit().equalsIgnoreCase(hand.get(i+1).getSuit())) {
            		i++;
            		continue;
            	}else {
            		return 0;
            	}
            }
            return 1;
    }
    
    public static int fullHouse(List<Card> hand){
        if (pair(hand)==2 && three(hand)==1 && four(hand)==0){
            return 1;
        }
        else{
            return 0;
        }    
    }    
    
    public static int four(List<Card> hand){
        if (pair(hand)==2 && three(hand)==1){
            if(hand.get(0).isSameValue(hand.get(3)) 
                        || hand.get(1).isSameValue(hand.get(4))){ 
                return 1;
            }
        }
        return 0;            
    }    
    
    public static int straightFlush(List<Card> hand){
        // Flush + Straight
        if (flush(hand)==1 && straight(hand)==1){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    public static int royalFlush(List<Card> hand){
        // Flush + 1st card is 10
        if (straightFlush(hand)==1 && hand.get(0).getValue()==10){
            return 1;
        }
        else{
            return 0;
        }
    }  
}
