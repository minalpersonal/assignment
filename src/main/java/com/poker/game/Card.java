package com.poker.game;

/**
 * Card and Utilities for Cards
 * @author
 *
 */
public class Card implements Comparable<Card>{

	private int value;
	private String suit;

	public Card(int value, String suit) {
		super();
		this.value = value;
		this.suit = suit;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getSuit() {
		return suit;
	}

	public void setSuit(String suit) {
		this.suit = suit;
	}

	public boolean isSameValue(Card c){
		
        return this.getValue()==c.getValue();
    }
	
	public static boolean areConsecutive(Card one, Card two){
		
        return two.getValue()-one.getValue() == 1;
    }
	
	public boolean sameSuit(Card c){
        return this.getSuit()==c.getSuit();
    }
	
	public static int getCardIntValue(String value){
		int intValue = 0;
		try {
			intValue= Integer.parseInt(value);
		}
		catch(NumberFormatException ex) {
			switch(value) {
			case "A":
				intValue= 14;
				break;
			case "T":
				intValue= 10;
				break;
			case "J":
				intValue= 11;
				break;
			case "Q":
				intValue= 12;
				break;
			case "K":
				intValue= 13;
				break;
			}
		}
		return intValue;
	}
	
	@Override
	public String toString() {
		 return this.value+this.suit;
	}
	
	public int compareTo(Card c){
        int answer=this.getValue()- c.getValue();
        
        return answer;
    }
}
