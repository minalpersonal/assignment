package com.poker.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import com.poker.utils.ValidationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * 
 * This class calculates the wins of two poker Players
 *
 */
public class PokerGameStarter {

	private static final Logger LOGGER = LoggerFactory.getLogger(PokerGameStarter.class);

	static int player1Win = 0;
	static int player2win = 0;
	static int ties = 0;

	public static void main(String[] args) {

		if (args.length != 1) {
			LOGGER.error("Invalid command line, exactly one argument required");
			System.exit(1);
		}

		if (args.length > 0) {
			
			String filePath = args[0];

			startGame(filePath);
			LOGGER.debug("Reading file : "+ args[0]);
			System.out.println("Player 1 : " + player1Win);
			System.out.println("Player 2 : " + player2win);
		}

	}

	/**
	 * Read each hand from the input file and decide the win
	 * @param fileName
	 */
	private static void startGame(String fileName) {
		List<String> lines = null;
		try {
			BufferedReader br = Files.newBufferedReader(Paths.get(fileName));
			lines = br.lines().map(line -> line).collect(Collectors.<String>toList());

			lines.forEach(new Consumer<String>() {

				public void accept(String line) {
					List<String> cards = new ArrayList<>(Arrays.asList(line.split(" ")));
					List<Card> cardsDealt = new ArrayList<Card>();
					if (ValidationUtil.validateHands(cards, cardsDealt)) {
						LOGGER.debug("Hand is ::" + "Valid");
						List<Card> player1Hand = cardsDealt.subList(0, cardsDealt.size() / 2);
						List<Card> player2Hand = cardsDealt.subList(5, cardsDealt.size());
						playGame(player1Hand, player2Hand);

					} else {
						LOGGER.debug("Hand is ::" + "Invalid");
					}
				}
			});

		} catch (IOException io) {
			LOGGER.error("Error Reading File: " + fileName);
			io.printStackTrace();
		}

		return;
	}

	private static void playGame(List<Card> player1Hand, List<Card> player2Hand) {

		LOGGER.debug("-------");
		logHand(player1Hand);
		int player1handResult = Hands.checkHand(player1Hand);
		logHand(player2Hand);
		int player2handResult = Hands.checkHand(player2Hand);

		if (player1handResult > player2handResult) {
			LOGGER.debug("Player 1 wins");
			player1Win++;
		} else if (player1handResult < player2handResult) {
			LOGGER.debug("Player 2 wins");
			player2win++;
		} else {

			int winningPlayer = TieResolution.resolveTie(player1Hand, player2Hand, player2handResult);
			if (winningPlayer == 1) {
				player1Win++;
			} else if (winningPlayer == 2) {
				player2win++;
			} else {
				ties++;
			}

		}
		LOGGER.debug("-------");
	}
	
	private static void logHand(List<Card> playerHand) {
		playerHand.forEach(new Consumer<Card>() {
			public void accept(Card t) {
				LOGGER.debug(" " + t);
			}
		});
	}

}
