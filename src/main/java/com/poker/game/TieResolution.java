package com.poker.game;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Resolution of Tie according to the document
 * @author
 *
 */
public class TieResolution {
	
	public static int resolveTie(List<Card> player1Hand, List<Card> player2Hand, int result) {
		
		int winningPlayer = 0;
		if(result == 3 || result == 7 || result == 2 || result == 4 || result == 8) {
			winningPlayer =resolvePairs(player1Hand, player2Hand, result);
		}else {
			winningPlayer= updateWin(player1Hand, player2Hand);
		}
		
		return winningPlayer;
	}
	
	private static int resolvePairs(List<Card> player1Hand, List<Card> player2Hand, int result) {
		
		int winningPlayer = 0;
		int player1commonCard =0;
		int player2commonCard =0;
		Set<Integer> common1 = new HashSet<Integer>();
		
		for(Card card: player1Hand) {
			
			if(!common1.add(card.getValue()) && player1commonCard<card.getValue()) {
				player1commonCard = card.getValue();
				break;
			}
			common1.add(card.getValue());
		}
		
		Set<Integer> common2 = new HashSet<Integer>();
		for(Card card: player2Hand) {
			
			if(!common2.add(card.getValue()) && player1commonCard<card.getValue()) {
				player2commonCard = card.getValue();
				break;
			}
			common2.add(card.getValue());
		}
		
		winningPlayer = player1commonCard==player2commonCard?updateWin(player1Hand, player2Hand):((player1commonCard>player2commonCard)?1:2);
		
		return winningPlayer;
	}
	
	public static int updateWin(List<Card> player1Hand, List<Card> player2Hand) {
		int winningPlayer = 0;
		
			int i = player1Hand.size() - 1;
			while (i >= 0) {
				if (player1Hand.get(i).getValue() > player2Hand.get(i).getValue()) {
					winningPlayer = 1;
					i--;
					break;
				} else if (player1Hand.get(i).getValue() < player2Hand.get(i).getValue()) {
					winningPlayer = 2;
					i--;
					break;
				}
				i--;
			}
		
		return winningPlayer;
	}
	
	
	
}
