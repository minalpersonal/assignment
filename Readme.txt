Follow the below instructions:

1. Build the application:
mvn clean install

2. Run the jar with dependencies from the target folder:
~\poker\target>java -jar poker-jar-with-dependencies.jar poker-hands.txt

3. Output is displayed on the console:
Player 1 : 263
Player 2 : 237

4. To analyse the hands in detail, check the log file generated at 'logs\app.log'

Enjoy!